class DataNode {
	// Pointers held for each node.
	static nodePointers = new WeakMap();

	// Pointers to all the nodes that is connected to this one.
	linkedNodes = [];

	// The data held by this node.
	data;

	// This Node's current position.
	position = [0,0];

	// The size of this node.
	size = 10;

	// This node's color
	color = "#000000";

	/**
	 * Construct a node.
	 * data - The data that this node will hold.
	 */
	constructor (data) {
		this.data = data;

		DataNode.nodePointers.set(data, this);

		// Assign a random color for testing.
		let hexNums = "0123456789ABCDEF";
		let color = "#";
		for (let i = 0; i < 6; i++) {
			color += hexNums[Math.floor(Math.random() * 16)];
		}

		this.color = color;
	}

	/**
	 * Global draw call, override this to change how all nodes will be
	 * drawn.
	 */
	static draw = ({ctx, position, size, color}) => {
		ctx.save();

		ctx.strokeStyle = color;
		ctx.beginPath();
		ctx.moveTo(position[0] + size, position[1]);
		ctx.arc(position[0], position[1], size, 0, 2 * Math.PI);
		ctx.stroke();

		ctx.restore();
	};

	/**
	 * Local draw call, override this to change how this node will be
	 * drawn. This call takes precedence over the global configureDraw.
	 */
	draw = (drawParams) => {
		// Default to the global draw options.
		DataNode.draw(drawParams);
	};

	/**
	 * The draw call that should be called to draw the node.
	 */
	defaultDraw (ctx) {
		this.draw({
			ctx,
			position: this.position,
			size: this.size,
			color: this.color
		});
	}

	/**
	 * This method defines the hitbox for this node.
	 */
	isPositionOnNode(x, y) {
		if (x < this.position[0] - this.size)
			return false;

		if (x > this.position[0] + this.size)
			return false;

		if (y < this.position[1] - this.size)
			return false;

		if (y > this.position[1] + this.size)
			return false;

		return true;
	}
}

export default DataNode;
