import OpenBrain from "./openbrain.mjs";

(() => {
	const canvas = document.getElementById("map");
	const ctx = canvas.getContext("2d");
	const canvasWidth = 1000;
	const canvasHeight = 600;

	canvas.width = canvasWidth;
	canvas.height = canvasHeight;

	canvas.style.width = canvasWidth + "px";
	canvas.style.height = canvasHeight + "px";

	const brain = new OpenBrain(canvas);

	let objs = [];

	for (let i = 0; i < 100; i++) {
		objs.push({
			title: i
		});
	}

	for (let i = 0; i <= 10; i++) {
		brain.addNode(objs[i]);
	}

	brain.makeLink(objs[0], objs[1]);
	brain.makeLink(objs[1], objs[2]);
	brain.makeLink(objs[2], objs[3]);

	brain.makeLink(objs[1], objs[5]);
	brain.makeLink(objs[1], objs[6]);
	brain.makeLink(objs[1], objs[7]);


	brain.makeLink(objs[5], objs[8]);
	brain.makeLink(objs[5], objs[9]);
	brain.makeLink(objs[5], objs[10]);

	brain.makeLink(objs[0], objs[3]);
	brain.makeLink(objs[0], objs[4]);

	const loop = () => {
		brain.draw();

		window.requestAnimationFrame(loop);
	};
	window.requestAnimationFrame(loop);

})();
