import DataNode from "./dataNode.mjs"

class OpenBrain {
	canvas;
	ctx;
	rootNode = null;

	constructor (canvas) {
		this.bindToCanvas(canvas);

		// Reset the draw map
		this.drawMap = new WeakMap();
	}

	bindToCanvas (canvas) {
		if (canvas) {
			this.canvas = canvas;
			this.canvas.addEventListener("click", e => this.onClick(e));

			this.ctx = canvas.getContext("2d");
		}
	}

	onClick (e) {
		let rect = this.canvas.getBoundingClientRect();
		let node = null;

		if (this.rootNode)
			node = this.findNodeAt(
				[],
				this.rootNode,
				e.x - rect.left,
				e.y - rect.top
			);

		if (node)
			this.rootNode = node;

		this.draw();
	}

	addNode (data) {
		let node = new DataNode(data);

		if (!this.rootNode) {
			this.rootNode = node;
		}
	}

	findNodeAt(searchedItems, node, x, y) {
		if (searchedItems.includes(node.data))
			return null;

		searchedItems.push(node.data);

		if (node.isPositionOnNode(x, y))
			return node;

		for (let i = 0; i < node.linkedNodes.length; i++) {
			const item = node.linkedNodes[i];

			const findChild = this.findNodeAt(searchedItems, item, x, y);

			if (findChild)
				return findChild;
		}

		return null;
	}

	removeNode () { }

	makeLink (data1, data2) {
		let dataNode1 = DataNode.nodePointers.get(data1);
		let dataNode2 = DataNode.nodePointers.get(data2);

		dataNode1.linkedNodes.push(dataNode2);
		dataNode2.linkedNodes.push(dataNode1);
	}

	draw () {
		// Clear canvas
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

		this.ctx.save();

		if (this.rootNode) {
			this.rootNode.position = [
				this.canvas.width / 2,
				this.canvas.height / 2
			];

			this.rootNode.defaultDraw(this.ctx);

			this.drawNodes([this.rootNode.data], this.rootNode);
		}

		this.ctx.restore();

	}

	drawNodes (drawnItems, node, depth = 1) {
		// Count the number of undrawn children nodes
		let undrawnCount = 0;

		for (let i = 0; i < node.linkedNodes.length; i++) {
			if (!drawnItems.includes(node.linkedNodes[i].data))
				undrawnCount++;
		}

		// Draw all children nodes
		let drawIndex = 0;

		for (const item of node.linkedNodes) {
			if (drawnItems.includes(item.data))
				continue;

			drawnItems.push(item.data);

			let rad = (drawIndex / undrawnCount) * 2 * Math.PI + (depth - 1) * Math.PI / 4;

			item.position = [
				node.position[0] + Math.cos(rad) * 120 / (depth / 2),
				node.position[1] + Math.sin(rad) * 120 / (depth / 2)
			];

			// Draw a line to this node
			this.ctx.beginPath();
			this.ctx.moveTo(node.position[0], node.position[1]);
			this.ctx.bezierCurveTo(
				node.position[0],
				node.position[1],

				item.position[0],
				item.position[1],

				item.position[0],
				item.position[1]
			);
			this.ctx.stroke();

			item.defaultDraw(this.ctx);

			drawIndex++;
		}

		// Draw the childrens children.
		if (undrawnCount != 0) {
			node.linkedNodes.forEach( (item, index) => {
				this.drawNodes(drawnItems, item, depth + 1);
			});
		}
	}

	breakLink () { }
}

export default OpenBrain;
